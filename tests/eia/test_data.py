# test the eia.data module

from pytest import mark
from requests import head

from nyspowcoo.eia.data import File, years


def test_url():
    assert (
        File(year=2021).url
        == "https://www.eia.gov/electricity/data/emissions/archive/xls/emissions2021.xlsx"
    )


@mark.slow
@mark.parametrize(argnames=["year"], argvalues=[(year,) for year in years])
def test_url_up(year: int):
    try:
        with head(File(year=year).url) as r:
            status_code = r.status_code
    except Exception:
        status_code = -1
    assert status_code == 200
