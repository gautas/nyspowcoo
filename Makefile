.PHONY: dev install download_eia_data save_nys_eia_data_to_csv serve

dev:
	docker login registry.gitlab.com/gautas/pydev -u local -p $(shell cat pat)
	docker run -it -d --rm --name pydev -v ~/dockvol:/dockvol -v ~/.gitconfig:/etc/gitconfig --env-file .env registry.gitlab.com/gautas/pydev:1.0.0

install:
	poetry install -E api

download_eia_data:
	poetry run python3 ./scripts/download_eia_data.py

save_nys_eia_data_to_csv:
	poetry run python3 ./scripts/save_nys_eia_data_to_csv.py

serve:
	poetry run uvicorn nyspowcoo.api.main:api
