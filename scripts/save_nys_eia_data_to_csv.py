# script to save all NYS EIA data to csv

# %%
from logging import info, basicConfig, INFO
from nyspowcoo.eia.data import State

# %%
basicConfig(level=INFO)

# %%
state = "NY"
info(f"saving {state} EIA data to csv: {State(state=state).fp_csv}")
State(state=state).save_csv()

