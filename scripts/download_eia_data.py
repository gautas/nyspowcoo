# script to download all EIA data

# %%
from logging import info

from nyspowcoo.eia.data import File, years

# %%
for year in years:
    file = File(year=year)
    info(f"downloading year {year}: {file.url} to {file.fp_local_xlsx}")
    File(year=year).save_xlsx()

