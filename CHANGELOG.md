# CHANGELOG


## v0.7.0 (2024-11-30)

### Continuous Integration

- Remove the build with previous version before re-building
  ([`17fd89d`](https://gitlab.com/gautas/nyspowcoo/-/commit/17fd89dcab08b2c368b99ec856c235bd7e1b6174))

- Re-build the package in the release stage
  ([`1de0ec2`](https://gitlab.com/gautas/nyspowcoo/-/commit/1de0ec2bcd3ff91fe11b858673fa8002830282b0))

### Documentation

- Merge branch '9-add-documentation-for-the-script-to-pull-nys-power-plant-data-into-csv' into
  'main'
  ([`a33dab7`](https://gitlab.com/gautas/nyspowcoo/-/commit/a33dab7b05c1848da9b22de740511d214f916354))

Resolve "add documentation for the script to pull NYS power plant data into csv"

Closes #9

See merge request gautas/nyspowcoo!5

- Clean up the README
  ([`3207b4b`](https://gitlab.com/gautas/nyspowcoo/-/commit/3207b4b294a7b51030b7b49049e1a4f7ef5ba31a))

### Features

- Update for new 2023 data from EIA
  ([`d96a3a9`](https://gitlab.com/gautas/nyspowcoo/-/commit/d96a3a919071cf5d694cd56ffcfcf5b9169fb665))


## v0.6.0 (2024-04-13)

### Continuous Integration

- Ignore slow tests
  ([`5f59c10`](https://gitlab.com/gautas/nyspowcoo/-/commit/5f59c10a2002ba805621396509f45e7f4d10352d))

- Add debug lines to release job
  ([`0bb975e`](https://gitlab.com/gautas/nyspowcoo/-/commit/0bb975eae8c0ef99c2206ba7aa39e53b2d5e070a))

### Features

- Add script to save ny data to csv
  ([`dac7bcc`](https://gitlab.com/gautas/nyspowcoo/-/commit/dac7bcc400d987dd26c137f7ce905b56701b4b37))


## v0.5.1 (2024-01-06)

### Bug Fixes

- Api: output eia data as json
  ([`6e9a406`](https://gitlab.com/gautas/nyspowcoo/-/commit/6e9a4069b816a37811ce98c11baaeb1582a9b62d))

### Chores

- Remove pat [skip ci]
  ([`1d22dc9`](https://gitlab.com/gautas/nyspowcoo/-/commit/1d22dc9bac7448ef660ba73cd091fa36d9d3afa1))

### Continuous Integration

- [skip ci] combine semver and publish steps
  ([`34cad86`](https://gitlab.com/gautas/nyspowcoo/-/commit/34cad86104762e0c2f0ca60a7ed750b8b67cdbbb))


## v0.5.0 (2024-01-06)

### Chores

- Clean up download script
  ([`de79bdb`](https://gitlab.com/gautas/nyspowcoo/-/commit/de79bdbaa2d6ade3b09278adf3fff6da445d4d1f))

### Continuous Integration

- Don't run build and test in tag or mr event
  ([`9eb907a`](https://gitlab.com/gautas/nyspowcoo/-/commit/9eb907adefb53824fcb8b7af9b72c19c2ee142bc))

### Features

- Api: add endpoint for the NYS eia data
  ([`53515b3`](https://gitlab.com/gautas/nyspowcoo/-/commit/53515b36824918bcbca71f4494ca9716113404a9))

- Filter to a state and combine across years
  ([`e26e42e`](https://gitlab.com/gautas/nyspowcoo/-/commit/e26e42e32e33092bbe9d19266d560d57cb9125d5))


## v0.4.0 (2024-01-05)

### Chores

- Clean up download script
  ([`0695300`](https://gitlab.com/gautas/nyspowcoo/-/commit/069530093324c82cd0fecd94de00df64d9e1c425))

- Add openpyxl dependency
  ([`e89cc50`](https://gitlab.com/gautas/nyspowcoo/-/commit/e89cc50e714cc0ad05758b585eb35581b69fa4e6))

- Add data loc as env var
  ([`ac0065a`](https://gitlab.com/gautas/nyspowcoo/-/commit/ac0065afc9acfabaf27c89103cdd721dc8149c78))

- Fix empty function
  ([`be30491`](https://gitlab.com/gautas/nyspowcoo/-/commit/be304911b8494dc05be2e22da6743186fe4fe485))

### Continuous Integration

- Fix install in test step
  ([`f5e87c7`](https://gitlab.com/gautas/nyspowcoo/-/commit/f5e87c7d6211ea0ff2186bede8edff47cd1c78b3))

- Update toml version on release
  ([`e3d771c`](https://gitlab.com/gautas/nyspowcoo/-/commit/e3d771c54adae5c9945ff889c8053e8607499390))


## v0.3.0 (2023-12-30)

### Features

- Update version in pyproject.toml
  ([`3edab1c`](https://gitlab.com/gautas/nyspowcoo/-/commit/3edab1c2232d23de512a6f8c89665bbbc71fd18c))


## v0.2.0 (2023-12-30)

### Features

- Start an api
  ([`18593b0`](https://gitlab.com/gautas/nyspowcoo/-/commit/18593b0e21d128a5f722b7d950c72717f178f5a2))
