# tools to work with eia data

from dataclasses import dataclass, field
from functools import cached_property
from logging import info
from pathlib import Path

from pandas import DataFrame, concat, read_csv, read_excel
from requests import get

from ..settings import Settings

url: str = "https://www.eia.gov/electricity/data/emissions/"

years_current: list[int] = [2023]
years_archive: list[int] = list(range(2013, 2023))
years: list[int] = years_current + years_archive


@dataclass
class File:
    year: int
    settings: Settings = field(default_factory=Settings)

    @property
    def filename(self) -> str:
        return f"emissions{self.year}.xlsx"

    @property
    def part_url_archive(self) -> str:
        return "archive/" if self.year in years_archive else ""

    @property
    def url(self) -> str:
        return f"{url}{self.part_url_archive}xls/{self.filename}"

    @cached_property
    def content(self) -> bytes:
        with get(self.url) as r:
            content = r.content
        return content

    @property
    def fp_local_xlsx(self) -> Path:
        return self.settings.loc_data / self.filename

    def save_xlsx(self) -> None:
        if not self.settings.loc_data.exists():
            self.settings.loc_data.mkdir()
        if self.fp_local_xlsx.exists():
            info(f"skipping save, {self.fp_local_xlsx} already exists")
            return
        info(f"saving EIA emissions data for year {self.year} to {self.fp_local_xlsx}")
        with open(self.fp_local_xlsx, "wb") as f:
            f.write(self.content)

    @cached_property
    def df_xlsx(self) -> DataFrame:
        if not self.fp_local_xlsx.exists():
            self.save_xlsx()
        return read_excel(self.fp_local_xlsx, sheet_name="CO2", skiprows=1)

    def df_xlsx_state(self, state: str = "NY") -> DataFrame:
        df_xlsx = self.df_xlsx
        return df_xlsx[df_xlsx["State"] == state]


@dataclass
class State:
    state: str = "NY"
    settings: Settings = field(default_factory=Settings)

    @cached_property
    def df_xlsx(self) -> DataFrame:
        return concat(
            [
                File(year=year).df_xlsx_state(state=self.state).assign(year=year)
                for year in years
            ]
        )

    @property
    def fp_csv(self) -> Path:
        return self.settings.loc_data / f"state_data_{self.state}.csv"

    def save_csv(self) -> None:
        if not self.settings.loc_data.exists():
            self.settings.loc_data.mkdir()
        if self.fp_csv.exists():
            info(f"skipping save, {self.fp_csv} already exists")
            return
        info(f"saving state {self.state} EIA data to {self.fp_csv}")
        self.df_xlsx.to_csv(self.fp_csv, index=False)

    @property
    def df(self) -> DataFrame:
        if not self.fp_csv.exists():
            self.save_csv()
        return read_csv(self.fp_csv)
