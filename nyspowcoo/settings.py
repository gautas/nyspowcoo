# project settings

from pathlib import Path

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    loc_data: Path = Path("./data")

    model_config = SettingsConfigDict(env_prefix="nyspowcoo_")
