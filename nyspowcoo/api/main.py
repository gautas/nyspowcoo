from json import dumps, loads

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from ..eia.data import State

api = FastAPI()

origins = [
    "https://gautsi.static.observableusercontent.com",
]

api.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@api.get("/")
async def root():
    return {"message": "Hello World to nyspowcoo"}


@api.get("/eia/")
def read_state():
    return loads(State(state="NY").df.to_json(orient="records"))
