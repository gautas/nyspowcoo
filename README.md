# nyspowcoo

Tools to analyze NYS power plant CO2 emissions

## Data
### EIA
- yearly plant-level emissions: https://www.eia.gov/electricity/data/emissions/

## Installation
From the [package registry](https://gitlab.com/gautas/nyspowcoo/-/packages/24500868), or download/clone and pip install.

## Usage
The script [scripts/save_nys_eia_data_to_csv.py](https://gitlab.com/gautas/nyspowcoo/-/blob/main/scripts/save_nys_eia_data_to_csv.py?ref_type=heads) shows how to use the package to extract a particular state's power plants across years to a csv. 

## Support
Feel free to open an [issue](https://gitlab.com/gautas/nyspowcoo/-/issues).

## Roadmap
- incorporate emission rates by fuel to isolate emissions from electricity generation 
- add calculations like excess emissions (emissions over the average emission rate) 
- serve the data via API 

## Contributing
Feel free to contribute. This project uses 
- [gitlab CI/CD](https://gitlab.com/gautas/nyspowcoo/-/pipelines) to automate build, test and publish,
- [poetry](https://python-poetry.org/) to manage dependencies, build and publish,
- [pytest](https://docs.pytest.org/en/8.0.x/) to test,
- [black](https://pypi.org/project/black/) and [isort](https://pypi.org/project/isort/) to format, and
- [python-semantic-release] to automate versioning and manage the changelog
    - use the [default commit parsing tags](https://python-semantic-release.readthedocs.io/en/latest/commit-parsing.html#built-in-commit-parsers) in your commit messages (e.g. "feat", "fix", "test", "docs", "ci")

## License
[MIT](https://gitlab.com/gautas/nyspowcoo/-/blob/main/LICENSE?ref_type=heads)

## Project status
The API part may not be working, but unclear when I'll turn back to this project to fix. The initial goal of this project was to facilitate analysis like [this](https://observablehq.com/@gautsi/nyspowcoo), which it has done.
